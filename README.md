# QA Exercises #

Hi there. Included below are a couple of exercises that are geared specifically for QA, there will be no coding needed whatsoever. You'll find instructions on what to setup and how to complete the exercise. Please keep in mind that all exercises are best-effort and are open to your interpretation in terms of what is being requested. There are no intentional "tricks", "gotchas" or brainteasers. Lastly, these exercises are not intended to consume your time, so please do not take more time than you think is reasonable.

## Exercise 1 ##

Below is a link to a repo we provide developer candidates as a back-end, API assessment. The link should display a README that includes tasks and requirements. We should have also sent you a link to an implementation of said API, specifically a Swagger page to use to test the API.

* https://bitbucket.org/hriinc/eval-fast-bk-1-37dk/src/master/

### Task ###

Please use the link we sent you to QA the API based on the tasks/requirements outlined in the linked README. We leave you to determine what is noteworthy or not. Please collect all of your QA feedback in one document that will be included in your email response submission.

### Instructions on how to authenticate with Swagger ###

The following video should walk you through how to authenticate with Swagger for further testing. If the video takes too long to play in the browser, please try downloading the video to your computer and playing it there.

* https://drive.google.com/file/d/1b5aEPhBXUfUupV2DLWKdUzG0unmiHyk3/view?usp=sharing


## Exercise 2 ##

Below are 2 links. The first is a link to a repo we provide developer candidates as a front-end assessment. The link should display a README that includes tasks and requirements. The second is a link to a zip file containing an implementation of that front-end assessment. Please unzip the file and open the `index.html` in the folder to view it.

* https://bitbucket.org/hriinc/eval-fast-fr-1-mz3y/src/master/exercises/interview-exercise-4/
* https://drive.google.com/file/d/15kvM6XuWT6SnXgsYjuxxOvKgPmmtzzOL/view?usp=sharing

### Task ###

Please use the implementation in that second link to QA the web page based on the tasks/requirements outlined in the front-end assessment README. We leave you to determine what is noteworthy or not. Please collect all of your QA feedback in one document that will be included in your email response submission.

## Submission ##

Please email us back with both feedback documents for the exercises. Thanks again for taking the time. If you have any trouble accessing any of the links we sent you, please just let us know.